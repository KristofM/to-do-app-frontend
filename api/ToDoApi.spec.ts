import { ToDoApi } from "./ToDoApi";
import { mockToDos } from "../lib-test/mock-todos";
import { ToDoApiImpl } from "./ToDoApiImpl";

describe("To Do API", () => {
  let toDoApi: ToDoApi;
  let backendUrl = "http://backend";
  let toDoEndpoint = "/to-dos";

  beforeEach(() => (toDoApi = new ToDoApiImpl(backendUrl, toDoEndpoint)));

  it("should fetch all to-dos from an HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest.fn().mockReturnValue(
      Promise.resolve({
        json: () => Promise.resolve(mockToDos),
      }) as Promise<Response>
    );
    global.fetch = fetchMock;

    // Test
    expect(await toDoApi.getAllToDos()).toEqual(mockToDos);
    expect(fetchMock).toHaveBeenCalledWith(`${backendUrl}${toDoEndpoint}`);

    // Cleanup
    fetchMock.mockClear();
  });

  it("should update a to-do using the HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest
      .fn()
      .mockReturnValue(Promise.resolve({ json: () => Promise.resolve() }));
    global.fetch = fetchMock;

    const updatedToDo = { id: 0, Name: "new name", Done: false };

    // Test
    await toDoApi.updateToDo(updatedToDo.id, updatedToDo);
    expect(fetchMock).toHaveBeenCalledWith(
      `${backendUrl}${toDoEndpoint}/${updatedToDo.id}`,
      {
        body: JSON.stringify(updatedToDo),
        headers: {
          "Content-Type": "application/json",
        },
        method: "PUT",
      }
    );

    // Cleanup
    fetchMock.mockClear();
  });

  it("should create a new to-do using the HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest
        .fn()
        .mockReturnValue(Promise.resolve({ json: () => Promise.resolve() }));
    global.fetch = fetchMock;

    const newTodo = { id: 0, Name: "new name", Done: false };

    // Test
    await toDoApi.createToDo(newTodo);
    expect(fetchMock).toHaveBeenCalledWith(
        `${backendUrl}${toDoEndpoint}`,
        {
          body: JSON.stringify(newTodo),
          headers: {
            "Content-Type": "application/json",
          },
          method: "POST",
        }
    );

    // Cleanup
    fetchMock.mockClear();
  });
});
