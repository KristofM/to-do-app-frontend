import styled from "styled-components";
import {FormEvent, useRef} from "react";
import {ToDo} from "../types/ToDo";
interface ToDoCreateItemProps {
    createToDo: (todo: ToDo) => void;
}

const Form = styled.form`
  display: flex;
  justify-content: flex-end;
  margin-top: 2em;
  margin-bottom: -4em;
`;

const Input = styled.input`
  border-radius: 12px;
  box-shadow: rgba(0, 0, 0, 0.5) 2px 2px 3px;
  margin-right: 12px;
  border: 1px solid gray;
  padding: 10px;
  width: fit-content;
  :focus-visible {
    outline: none;
  }
`;

const Button = styled.button`
  border-radius: 12px;
  box-shadow: rgba(0, 0, 0, 0.5) 2px 2px 3px;
  background-color: orange;
  border-color: transparent;
`;

export default function ToDoCreateItem({createToDo}: ToDoCreateItemProps): JSX.Element {

    const nameInputRef = useRef<HTMLInputElement>(null);
    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const todoName = nameInputRef.current?.value;
        if(!todoName) {
            return
        }
        createToDo({
            Name: todoName,
            Done: false
        } as ToDo);
        (event.target as HTMLFormElement).reset();
    }

    return (
        <Form onSubmit={ handleSubmit }>
            <Input type="text" placeholder="Please enter a to-do name..." ref={nameInputRef}></Input>
            <Button type="submit">
                Create To-Do
            </Button>
        </Form>
    );
}
