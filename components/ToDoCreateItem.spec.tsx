import {fireEvent, render, RenderResult,} from "@testing-library/react";
import {ToDo} from "../types/ToDo";
import ToDoCreateItem from "./ToDoCreateItem";
import userEvent from "@testing-library/user-event";

describe("ToDoCreateItem", () => {
    let renderResult: RenderResult;
    let todoNameInput: HTMLInputElement;
    let addNewTodoButton: HTMLButtonElement;
    let mockCreateFunction: (todo: ToDo) => void;

    beforeEach(() => {
        mockCreateFunction = jest.fn();
        renderResult = render(
            <ToDoCreateItem createToDo={mockCreateFunction} />
        );
        todoNameInput = renderResult.container.querySelector(
            "input[type=text]"
        ) as HTMLInputElement;
        addNewTodoButton = renderResult.container.querySelector(
            "button"
        ) as HTMLButtonElement;
    });

    describe('elements rendered', () => {
        it("should render an input for the todo name", () => {
            expect(todoNameInput).toBeTruthy();
        });

        it("should render a button to create todos", () => {
            expect(addNewTodoButton).toBeTruthy();
        });
    });

    describe('submitting todos', () => {
        it("should create a new todo on button click", () => {
            expect(mockCreateFunction).not.toHaveBeenCalled();
            const todoName = 'new Todo';
            todoNameInput.value = todoName
            fireEvent.click(addNewTodoButton);
            expect(mockCreateFunction).toHaveBeenCalledWith({Name: todoName, Done: false});
        });

        it("should create a new todo on form submit", () => {
            expect(mockCreateFunction).not.toHaveBeenCalled();
            const todoName = 'something to do';
            userEvent.type(todoNameInput, `${todoName}{enter}`);
            expect(mockCreateFunction).toHaveBeenCalledWith({Name: todoName, Done: false});
        });

        it("should not create todos when name is empty", () => {
            expect(mockCreateFunction).not.toHaveBeenCalled();
            const todoName = '';
            userEvent.type(todoNameInput, `${todoName}{enter}`);
            expect(mockCreateFunction).not.toHaveBeenCalled();
            fireEvent.click(addNewTodoButton);
            expect(mockCreateFunction).not.toHaveBeenCalled();
        });
    });
});
