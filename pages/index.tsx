import type {GetServerSidePropsResult, NextPage} from "next";
import {useState} from "react";
import {ToDoList} from "../components/ToDoList";
import {ToDo} from "../types/ToDo";
import {ToDoApiFactory} from "../api/ToDoApiFactory";
import ToDoAddNew from "../components/ToDoCreateItem";

const toDoApi = ToDoApiFactory.get();

interface HomepageProps {
  toDos: ToDo[];
}

export async function getServerSideProps(): Promise<
  GetServerSidePropsResult<HomepageProps>
> {
  return {
    props: {
      toDos: await toDoApi.getAllToDos(),
    },
  };
}

const Home: NextPage<HomepageProps> = ({
  toDos: toDosFromProps,
}: HomepageProps) => {
  const [toDos, setToDos] = useState<ToDo[]>(toDosFromProps);

  async function updateToDoDoneStatus(
    id: number,
    done: boolean
  ): Promise<void> {
    const toDo: ToDo | undefined = toDos.find((toDo) => toDo.id === id);
    if (!toDo) {
      return;
    }

    const updatedToDo: ToDo = {
      ...toDo,
      Done: done,
    };

    setToDos((toDos) => [
      ...toDos.map((toDo) =>
        toDo.id === id && updatedToDo ? updatedToDo : toDo
      ),
    ]);
    await toDoApi.updateToDo(id, updatedToDo);
  }

  async function createToDo(todo: ToDo): Promise<void> {
    const newTodo: ToDo = await toDoApi.createToDo(todo);
    setToDos((toDos) => [
      ...toDos,
      newTodo
    ]);
  }


  return (
      <>
        <ToDoAddNew createToDo={createToDo}></ToDoAddNew>
        <ToDoList toDos={toDos} updateToDoDoneStatus={updateToDoDoneStatus} />
      </>
  );
};

export default Home;
